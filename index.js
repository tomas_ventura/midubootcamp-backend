const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors())
app.use(express.json())

app.use((req, res, next) => {
    console.log(req.method)
    console.log(req.path)
    console.log(req.body)
    console.log("pasa por aca y redirigimos a la siguiente")
    next()
})

let posts = [
    {
        id: 1,
        title: 'HTML is easy',
        body: '2019-05-30T17:30:31.098Z',
    },
    {
        id: 2,
        title: 'HTML is easy',
        body: '2019-05-30T17:30:31.098Z',
    },
    {
        id: 3,
        title: 'HTML is easy',
        body: '2019-05-30T17:30:31.098Z',
    }
]

// const app = http.createServer((request, response) => {
//     response.writeHead(200, {'Conten-Type': "application/json"})
//     response.end(JSON.stringify(NOTES))
// })

app.get('/', (req, res) => {
    res.send("<h4>Hola mundo<h4>")
})

app.get('/api/posts', (req, res) => {
    res.json(posts)
})

app.get('/api/posts/:id', (req, res) => {
    const id = req.params.id
    const post = posts.find(post => post.id == id)
    if (post) {
        res.json(post)
    } else {
        res.status(404).end()
    }
})

app.post('/api/posts', (req, res) => {
    console.log(req.body)
    newNote = {
        id: posts.length + 1,
        title: req.body.content,
        body: req.body.body
    }
    posts = posts.concat(newNote)
    res.status(201).json(posts)
})

app.delete('/api/posts/:id', (req, res) => {
    const id = req.params.id
    const posts = posts.filter(post => post.id != id)
    res.status(204).end()
})

app.use((req, res) => {
    res.status(404).json({
        //lo ponemos al final porque si no entra en cualquier peticion, va a entrar aca si o si
        error: `${req.path} no es un endpoint conocido`
    })
})

const PORT = 3001
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})